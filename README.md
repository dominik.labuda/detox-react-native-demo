# Detox tests for React Native app template

Working with XCode 12.4 and iOS 14.4 on iPhone 11 simulator.

## Running Detox tests

After `yarn install` follow the steps below.

### iOS

Install the required pods:
```bash
pushd ios && pod install && popd
```

Build the app:
```bash
npx detox build --configuration ios
```

The actual `xcodebuild` command that is executed with `detox build` can be found in [.detoxrc.json](.detoxrc.json).

Run the detox tests:
```bash
npx detox test --configuration ios
```